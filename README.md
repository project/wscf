## WSCF

WSCF a.k.a. Website Carbon Footprint module aims to display the carbon
footprint information of a Drupal site using the api.websitecarbon.com API.

## Installation

1 - Install the module and it's dependencies as usual.

2 - Add a block on the structure section to display the information extracted
    from the API. Recommended only on the main page.

3 - No additional configuration is required. The url passed is getting via the
    getSchemeAndHttpHost() method.

## Functionality

* Creates a block that can be added into any section of the block structure.
* Connects in async mode with the api.websitecarbon.com API and retrieve the
  information according to the results from the API.
	
## Developer

2023 - Eduardo Arana

## Thanks

Special thanks to the Drupal community to help me improve this module.
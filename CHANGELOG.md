# Changelog

## 1.2 (Oct 18, 2023)

- Fixed an issue reported by PHPCS.

## 1.1 (May 2, 2023)

- hook_help was missing, now is implemented. Thanks to gayatri chahar.
- Fixed the issues reported by PHPCS. Thanks to urvashi_vora and Mahima_Mathur23.

## 1.0 (March 22, 2023)

- Initial release of the module.

<?php

namespace Drupal\wscf\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

/**
 * Provides a block that uses twig to render its content.
 *
 * @Block(
 *   id = "wscf",
 *   admin_label = @Translation("Website Carbon Footprint"),
 *   category = "Sustainability"
 * )
 */
class WscfTwig extends BlockBase {

  /**
   * The api response and return the output in json format.
   *
   * The getCarbonFootprint function that uses promise in async mode, waits for.
   */
  public function getCarbonFootprint($url) {
    $client = new Client();
    $promise = $client->getAsync('https://api.websitecarbon.com/site', [
      'query' => ['url' => $url],
    ]);
    return $promise->then(function ($response) {
      return json_decode($response->getBody(), TRUE);
    }, function ($exception) {
      // Handle exception.
      return $exception;
    });
  }

  /**
   * Build function that pass the url to getCarbonFootprint.
   *
   * And waits for the response.
   * Once the api answer will pass the content to the carbon_footprint structure
   * in the wscf template.
   */
  public function build() {
    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    $promise = $this->getCarbonFootprint($base_url);
    $results = Promise\settle($promise)->wait();
    $carbonFootprintJson = json_encode($results[0]['value']);
    $carbonFootprintout = json_decode($carbonFootprintJson, TRUE);
    return [
      '#theme' => 'wscf',
      '#text' => 'Live Carbon Footprint Sustainability Data',
      '#carbon_footprint' => $carbonFootprintout,
    ];
  }

}
